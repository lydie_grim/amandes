# Introduction

- Enjeu écologique
    + Derèglement climatique, destruction des écosystèmes, extinction d'espèces (6ème extinction de masse)
    + Rappels de quelques chiffres
	   - Chiffres du rapport du GIEC (2014) : Rechauffement climatique + 4.8° d'ici 2100, Montée des eaux + 98cm d'ici 2100
- L'écologie politique est un ensemble de courants, largement diffusé depuis les années 1970, qui insiste sur la prise
  en compte des enjeux écologiques dans l'action politique et dans l'organisation sociale
- Différentes approches de l'écologie politique
- Situation actuelle : mouvement des jeunes en Allemagne, Suède, Belgique, France, etc. Marches pour le climat.
    + Nécessité de s'organiser collectivement, remise en cause des petits gestes comme solution suffisante, bien que la plus accessible
- Qui produit quoi ? Pourquoi ? Comment ? Pour qui ?


# L'écologie sous mode de production capitaliste

Pub rigolote (Danone) : https://twitter.com/DanoneFR/status/1039881515805683712

## Exemple du recyclage : Coca-Cola et ses bouteilles

Dans la première moitié du XX ème siècle, système de consigne pour la vente de boissons : on réutilisait la bouteille récupérée en la remplissant à nouveau de boisson après l'avoir lavée (ce qui est différent du recyclage).
Mais dans les années 1930, invention de la canette en métal, avec des avantages économique pour l'industrie de la boisson : réduction des coûts de production car suppression des coûts de collecte et de reconditionnement, élimine les intermédiaires, concentre la production.
Dés les années 1950, les fabriquants de soda (dont Pepsi et Coca-Cola) basculèrent vers la canette, ce qui accrut la production de déchets. En 1947, 100% des sodas et 85% des bières étaient vendues dans des bouteilles réutilisables. En 1971, ces chiffres sont passés à 50% et 25% reciproquement.

Devant le problème des déchets qui devenaient de plus en plus nombreux, des mouvements firent pression sur les autorités.
Par exemple : en 1953, l'État du Vermont adopta une loi rendant obligatoire le système de consigne.
     ----> les industriels sont directement pointés du doigt
La même année (1953) : création du consortium Keep America Beautiful réunissant des industriels de la boisson et de l'emballage, dont Coca-Cola.

Ce consortium fut à l'origine de campagnes publicitaires visant à populariser le recyclage avec pour mots d'ordres : "tout est entre vos mains, vous avez le pouvoir de faire la différence".

Et des exemples de lobbies du grand patronat pour empêcher les efforts sur l'écologie, il y en d'autres : on peut citer la Global Climate Coalition ou Business Europe plus récemment dont un memo a fuité, destiné au lobbyistes pour argumenter au parlement européen, dans lequel on peut lire : «Etre plutôt positif tant qu’il s’agit d’une déclaration politique qui n’a pas d’implications pour la législation européenne en vue de 2030», «s’opposer à l’augmentation des ambitions, en utilisant les arguments habituels que nous ne pouvons agir seuls dans un marché mondialisé et qu’on ne peut pas compenser pour les autres, etc.», «remettre en question le processus de décision en demandant plus de transparence dans les calculs, la réalisation d’une étude d’impact, le risque de créer de l’instabilité».

Le fait que des industriels aient choisis de se réunir en un conglomérat, capable d'agir de façon concerté, n'a rien d'anodin : il s'agit de l'organisation d'une classe sociale, la classe capitaliste, qui cherche à engranger un maximum de bénéfices, au détriment de la planète et sans se soucier du long terme.

En effet, dans nos sociétés, la production s'oriente vers le profit et non pas vers les besoins humains. C'est pour cela qu'on y observe frequemment des scandales sanitaires qui auraient souvent pu être évités, ainsi que des abérations écologiques.

Exemples :

- chlordécone, amiante, glyphosate, etc
- Obsolescence programmée
- Gachis à Amazon
- Monsanto : un agriculteur achetant des graines Monsanto, doit forcément payer chaque année pour pouvoir replanter ce qu'il a récolté.


## La décroissance
- Eloge de la pauvreté (religieuse) / posture morale et puritaine
- Détournement de la contradiction Capital/Nature au profit d'une contradiction Nature/Progrès technique


## Rôle des États
- Pétition pour attaquer les États en justice (+ de 2M signatures)
- Docu sur la tomate
    + États imperialistes qui dictent leurs politique au niveau mondial
- Les COP
    + Freiner le developpement des pays émergeants sous couvert d'écologie (COP 21)
		
## Mais alors, comment éviter la catastrophe ?
Ni les États, ni les grandes entreprises n'ont intérerêt à prendre en charge la question écologique.
De plus, le patronnat n'hésite pas à s'organiser autour de ses intérêts de classe.

Ces intérêts sont incompatibles avec ceux des travailleurs, non seulement du point de vue écologique, car ils sont victimes de la pollution, et consomment des produits dangereux sans avoir leur mot à dire, mais également du point de vue des conditions de travail, de la répartition des richesses, etc.

Tous ces problèmes sont liés : on ne peut pas dissocier la question écologique de la question sociale, et beaucoup de gens semblent en avoir pris conscience, cf le mot d'ordre qui semble souvent repris "Fin du monde, fin du mois, même combat !".
C'est pourquoi, il s'agirait d'en finir avec le capitalisme, mode de production obsolète, incapable de relever l'enjeu environemental, et de la remplacer pour un autre système, dans lequel le pouvoir est detenu par le plus grand nombre.

Bien sûr, tous les problèmes environnementaux et technologiques ne seront pas écartés par enchantement avec la fin du capitalisme.
Le réchauffement climatique ne va pas s'arrêter de sitôt par exemple. Même débarrassée des gaspillages et de l'absurdité du
capitalisme, l’humanité aura des choix à faire en matière de gestion de l'environnement, d'occupation de l'espace, de répartition de l'activité.

Mais, sous une société communiste, l'humanité pourra faire ses choix et trouver ses réponses à apporter aux problèmes environnementaux de manière consciente et démocratique via le contrôle des travailleurs sur l'économie.
Ainsi, les ouvriers et les salariés, au coeur de la production, pourront décider eux-mêmes de ce qu'on produit, comment, pour qui, etc.
Débarasser des injonctions à toujours plus de rentabilité, on pourra alors planifier selon les besoins humains, et rationaliser la production pour éviter de détruire la planète.



# Divers
- Changement climatique : https://wiki.datagueule.tv/Le_changement_(climatique)_c%27est_maintenant_(EP.48)
- Les etudiant.e.s contre le greenwashing - Communiqué des élèves de l’ENS, 20 février 2019 http://sauvonsluniversite.fr/spip.php?article8495
- Capitalisme : victoire par chaos climatique : https://peertube.datagueule.tv/videos/watch/193124a4-5b88-486e-990d-4b21a1f8a129
- Oubliez les douches courtes : https://www.youtube.com/watch?v=QqnC2avyNAk

# Sources
- Rapport du GIEC : https://www.lemonde.fr/les-decodeurs/article/2014/11/04/climat-5-rapports-du-giec-5-chiffres-alarmants_4517326_4355770.html
- Article sur Coca-Cola et le recyclage : https://www.monde-diplomatique.fr/2019/02/CHAMAYOU/59563
- Le groupe lobbyiste Global Climate Coalition : https://en.wikipedia.org/wiki/Global_Climate_Coalition
- Fuite du mémo de Business Europe : https://www.liberation.fr/planete/2018/09/19/quand-le-lobby-du-patronat-europeen-veut-minimiser-les-efforts-climatiques_1679840 et https://www.bastamag.net/Un-document-revele-comment-les-multinationales-entravent-toute-action
- Vidéo de gâchis à Amazon : https://www.minimachines.net/actu/amazon-le-grand-gachis-75009
- Brève de LO sur le chlordécone : https://www.lutte-ouvriere.org/breves/chlordecone-la-loi-du-profit-tue-113834.html
- Article wikipedia sur l'obsolescence programmée : https://en.wikipedia.org/wiki/Planned_obsolescence
- Vidéo sur Monsanto : https://wiki.datagueule.tv/MONSANTO_:_sa_vie,_son_empire_(EP.6)
- Article sur Pierre Rabhi et la décroissance : https://www.monde-diplomatique.fr/2018/08/MALET/58981
- Marx écologiste : http://www.editionsamsterdam.fr/marx-ecologiste/
