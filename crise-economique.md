## Résumé

Après plusieurs mois de crise sanitaire, la réponse du gouvernement est plutôt
claire : d'un côté, quelques miettes pour répondre aux demandes du personnel
hospitalier et des millions de gens qui se sont appauvris, et de l'autre, des
centaines de milliards d'euros accordés aux grosses entreprises alors même que
certaines prévoient des plans de licenciements ou renvoient les gens au boulot
sans garantir des conditions sanitaires à la hauteur. La crise économique
engendrée par l'épidémie a déjà créé une forte augmentation du nombre de
chômeurs, et il va être de plus en plus difficile de trouver un contrat stable.
Mais le gouvernement, déjà impopulaires à cause de sa politique contre les
classes populaires, est contesté de toutes parts. C'est pas aux travailleurs et
aux jeunes de payer la crise !

# TOPO du 10 septembre

## Introduction

L'épidémie de Covid-19 a déstabilisé, en quelques semaines, de larges pans de
l'économie mondiale : arrêt de la production, problèmes d'approvisionnement,
circulation des marchandises perturbés, baisse de la demande dans certains
secteurs, ...

Cette dérégulation n'a pas tardé à nous plonger dans une nouvelle crise
économique près de 10 ans seulement après celle de 2008.  Comme durant chaque
crise économique, les annonces de licenciements par les grands groupes
capitalistes ne se sont pas faites attendre.  En France, ces plans de
licenciements se traduisent par des milliers d'ouvriers et d'employés mis sur le
carreau chez Renault, Air France, Airbus, et beaucoup d'autres.

En avril ce ne sont pas moins de 843 000 personnes qui se sont inscrites à Pôle
emploi, soit une hausse de 22,6% par rapport à mars.  Aux États-Unis, où il n'y
a pas eu de mesure de chômage partiel, ce sont 30 millions de personnes qui se
sont inscrites pour bénéficier de l'assurance chômage, depuis le début de la
crise, auxquels il faut rajouter plusieurs millions de personnes qui ne sont pas
parvenu à faire leur demande ou qui n'y ont pas le droit.

Il y aurait beaucoup à dire sur les manifestations antiracistes qui ont parcouru
les Etats-Unis ces derniers temps, ou encore sur la situation au Liban, qui sont
toutes deux liées au contexte de crise sanitaire que nous traversons. Même si la
période est riche d'enseignements nous allons nous concentrés aujourd'hui sur la
façon dont l'Etat a répondu à la crise sanitaire et économique.



## Les travailleurs en première ligne contre l'épidémie

Alors que des décennies de mesures d'austérité ont mis les hôpitaux à genoux, le
gouvernement a été obligé, comme dans de nombreux pays, d'avoir recours au
confinement de la population. Durant cette période, alors qu'une partie de la
production était mise à l'arrêt, la société ne s'est pas complétement arrêtée.
Tout un tas de boulots, d'habitude peu considérés, se sont alors retrouvés sur
le devant de la scène médiatique.  Les plateaux télé et les journaux se sont
soudainement mis à évoquer les soignantes, éboueurs, caissières, facteurs,
agriculteurs, les travailleurs et travailleuses dans la logistique, la
manutention, la livraison, ...  Autant de personnes qui ont continué à faire
tourner la société quand une partie de la population était confinée.  Ce sont
les mêmes qui ont également été les plus exposés au virus : entre le 8 mai et le
4 août environ 50% des clusters sont apparus sur les lieux de travail.

Pour faire respecter des mesures sanitaires et faire fermer les boîtes non
essentielles durant le confinement il a parfois fallut se bagarrer.

[TODO : A rajouter]

D'ailleurs Anthony Smith, inspecteur du travail, a été suspendu il y a
quelques mois pour avoir averti une entreprise de service à la personne du
manque de protections des aides à domicile envoyées chez des personnes âgées.



### Dans les hôpitaux

Le secteur de la santé a été largement mobilisé ces derniers mois pour faire
face au virus, et les soignants ont été soumis à des cadences infernales.

Mais cette situation à l'hôpital est avant tout le résultat de coupes
budgétaires menées par les gouvernements de gauche comme de droite depuis
des années.
D'ailleurs la crise sanitaire, si elle a conduit à une dégradation des
conditions de travail dans la santé, a en fait aggravé des problèmes qui
existaient déjà avant.

Le personnel hospitalier avait d'ailleurs exprimé sa colère dans la rue le
14 novembre dernier, soit plusieurs mois avant le début de l'épidémie en France.
Et puis il y a eu les grèves dans les urgences l'été 2019 qui ont eu lieu un peu
partout dans le pays.

Les revendications des salariés de l'hôpital sont claires : revalorisation des
salaires d'au moins 300 euros, embauches massives, ouverture de lits,
titularisation.
Mais en réponse à ces revendications, le gouvernement n'a proposé qu'une
nouvelle tentative de négociations.
Après le "grand débat" pour tenter de calmer la colère des gilets jaunes,
la conférence de financement des retraites, où le gouvernement avait invité
les syndicats à s'asseoir à la table des négociations, c'est cette fois le
Ségur de la santé que nos dirigeants ont sorti de leur chapeau !
Ce Ségur de la santé, où les médecins étaient d'ailleurs surrepresentés par
rapport aux salariés plus bas dans la hiérarchie, n'avait bien sûr pas pour
but de répondre aux besoins des travailleurs de la santé, mais de tenter
d'éteindre la mèche de la contestation hospitalière.

D'ailleurs une des premières préoccupations d'Olivier Véran, ministre de la
santé, lors d'un entretien en mai, étaient bien d'avantage de revenir sur les
35h dans l'hôpital.
"Il ne s'agit pas d'obliger des gens à travailler davantage, mais de créer
un cadre beaucoup plus souple pour permettre à ceux qui le souhaitent de
le faire, ou d'organiser leur temps de travail différemment. Sans pression".
Bref, une n-ième mouture du "travailler plus pour gagner plus", alors même
que de nombreux travailleurs de l'hôpital dépassent déjà les 35h par semaine.

Et nous voilà finalement en septembre, toujours sans les embauches et les
ouvertures de lits réclamées, si bien qu'une deuxième vague de l'épidémie
laisserait les hôpitaux dans la même situation qu'en mars/avril dernier.



## Les vagues de licenciements, les plans du patronnat

Dans le reste des secteurs, du public comme du privé, la situation et les
perspectives d'avenir se sont beaucoup dégradées en quelques semaines.  En tout
cas c'est le cas pour beaucoup, mais pas pour tout le monde. Ainsi, Alexandre
Bompard PDG de Carrefour assurait en mai dernier que son entreprise "saura
saisir les opportunités nées de la crise".  Il entend peut-être par là
l'augmentation du prix du masque à l'unité, passé subitement de 7 centimes à
plusieurs dizaines de centimes ! Mais cette situation n'est pas spécifique à la
France. Ainsi, durant la dernière semaine d'août, les 500 plus grosses fortunes
mondiales ont gagné 175 milliards d'euros !

Cette crise aura permis aux grands patrons d'accélerer des restructurations déjà
prévues avant l'épidémie. À Renault, par exemple, l'annonce des 15 000
suppressions de postes arrivent dans un contexte où la boîte essaye de se
tailler une place dans le marché des voitures électriques et des voitures
autonomes. Comme souvent, il faut ajouter, en plus de ces suppressions, les
dizaines de milliers de sous-traitants dont l'activité dépend directement de
Renault.

Les salariés de Renault ont rapidement contesté cette décision. Dans les
fonderies de Bretagne et dans l'usine de Choisy-le-Roi, ils se sont mis en
grève. À Maubeuge c'est une manifestation réunissant 8 000 personnes à la sortie
du confinement, auxquels ont participé des travailleurs de Renault et des
habitants de la ville, qui a démontré leur détermination à ne pas se laisser
faire.

En plus des licenciements le patronat souhaite faire retourner les gens au
boulot, peu importe les risques de propagation du virus. Cette volonté n'est
même pas masquée par la ministre du Travail, Elisabeth Borne, qui déclarait il y
a quelques jours par rapport aux nombreuses fermetures de classes depuis la
rentrée scolaire : "la priorité ce sont des gardes d’enfants pour que les
parents puissent continuer à travailler". Au moins c'est clair.

Le gouvernement et le MEDEF veulent également s'attaquer à la durée du temps de
travail, nous expliquant qu'il faudra se serrer la ceinture pour faire face à la
crise, entamant en coeur la petite comptine du chantage à l'emploi.

- https://www.convergencesrevolutionnaires.org/A-la-valse-des-licenciements-Renault-ouvre-le-bal?archive=1
- https://www.convergencesrevolutionnaires.org/Le-confinement-s-arrete-les-licenciements-se-multiplient-et-la-lutte-des?archive=1


### Les APC

En matière de chantage à l'emploi, les APC, pour Accords de Performances
Collectives, ouvrent une nouvelle voix légale pour s'attaquer aux
travailleurs. Ce dispositif a été introduit dans le code du travail en 2017 avec
les « ordonnances Macron ». Il consiste à demander aux salariés d’accepter une
dégradation des conditions de travail en échange d’une promesse de maintenir
l’emploi. En réalité les entreprises ne sont même pas obligées de justifier des
difficultés économiques.

Avec les APC, ce sont des baisses de salaires, la remise en cause du nombre de
jours de RTT ou encore l'augmentation ou la baisse du temps de travail qui
peuvent être imposées, pour peu qu'une ou plusieurs organisation syndical ayant
recueillies plus de 50 pourcents des votes aux élections professionnelles
ratifient l'accord. Une contrainte qui n'en est pas toujours une, dans certaines
boîtes où les representants syndicaux sont d'avantage prêt à négocier avec la
direction plutôt qu'à défendre les employés. Ainsi, au sein du journal L'Équipe
ou chez Ryanair, ces accords ont entrainer une perte de jours de RTT ou des
baisses de salaires. Depuis 2017, c'est 300 APC qui ont été signées.

Récemment, chez Derichbourg, un sous-traitant d'Airbus, la direction a utilisé
un APC pour diminuer les salaires d'environ 500 euros par mois ! Plus de 150
travailleurs de l'entreprise ont refusé de signer cet "Accord Pour Crever",
comme il l'ont surnommé, et se sont retrouvés à la porte.

- https://www.convergencesrevolutionnaires.org/Les-APC-ou-comment-profiter-de-la-crise-pour-attaquer-les-travailleurs?archive=1


Non seulement le patronat fait la guerre aux travailleurs, mais l'Etat fait
passer des lois pour lui faciliter la tâche, que ça soit les accords de
performance collective, la réforme de l'assurance chômage, etc. Mais ce n'est
pas une coïncidence, et tous les gouvernements, de Mitterand à Macron, en
passant par De Gaulle et tous les autres, se sont rangés dans le camps des
capitalistes.



### Pourtant le patronnat arrosé d'argent publique

C'est ce même Etat qui a débloqué au printemps dernier 470 milliards d'euros en
cadeaux aux grands patrons. Le gouvernement vient de rajouter 100 milliards au
pactole, sous forme d'un plan de relance, censé garantir 160 000 embauches, à
comparer aux 1 million de salariés qui auront perdu leur emploi en 2020. Il
faudra donc se contenter d'une promesse de Jean Castex, le même genre de
promesse qu'on nous avait déjà faite sous Hollande avec le CICE (crédit d’impôt
pour la compétitivité et l’emploi), qui n'avait pas créé les emplois escomptés.


### Du côté de l'opposition : ça grenouille dans le marigot

Du côté de la gauche, les diverses formations politiques qui se proposent de
gouverner différement de Macron ont mis leurs revendications au placard durant
le confinement. Ainsi, à l'heure où des travailleurs des transports, de la Poste
ou d'Amazon exerçaient leur droit de retrait car ils estimaient qu'ils n'avaient
pas suffisamment de protection au travail pour faire face à l'épidémie, Olivier
Faure, premier secrétaire du PS disait avoir pour priorités de "ne pas affaiblir
le gouvernement". Du côté du PCF, Fabien Roussel, secrétaire national, a déclaré :
« On n’est pas à l’heure de l’opposition, on est à l’heure de trouver des solutions.
Après, le temps viendra de demander des comptes. »
Ce printemps 2020 nous aura montré que quand il s'agit de
préserver l'ordre social, les partis de droite et de gauche sont capables de se
mettre d'accord.

Nous avons donc vécu un confinement sous le signe de l'union nationale, l'heure
n'était pas à la contestation, pas à la revendication, il fallait se ranger
derrière la Nation. Du moins c'est ce que nous ont expliqué tous les politiciens
qui se proposent de reprendre les rênes du pays aux prochaines élections,
c'est-à-dire de défendre les intérêts des grandes firmes françaises.

- https://www.convergencesrevolutionnaires.org/Ca-grenouille-dans-le-marigot-de-la-gauche?archive=1

- Relocalisations ? https://www.convergencesrevolutionnaires.org/Relocalisations-Plutot-des-luttes-multinationales?navthem=1


## Le rôle de l'Etat dans les sociétés de classes

Car l'Etat, dans les sociétés sous mode de production capitaliste, c'est avant
tout un outil utilisé par la classe dirigeante pour maintenir son pouvoir. En
effet, l'Etat fournit un arsenal juridiques et une police pour réprimer les
pauvres et mater toutes celles et ceux qui relèvent la tête. De nombreuses
expériences historiques durant les 150 dernières années nous ont montré que,
pour un réel changement de société, il ne suffit pas de changer de gouvernements
ou de majorité parlementaire. De même, changer le numéro de la République,
fonder une 6ème ou une 7ème République, ne nous débarassera pas des inégalités
et de l'exploitation. Les contradictions et les différences d'intérêts au sein
de la société ne peuvent pas se résoudre sous le capitalisme.




## Un plan de bataille pour la jeunesse et le monde du travail

Le rôle des révolutionnaires dans cette période est non seulement de proposer à
la partie la plus combative de la jeunesse et du monde du travail de s'organiser
pour faire face aux offensives, mais également de dénoncer les illusions
propagées par certains partis réformistes, qui une fois au pouvoir méneraient la
même politique antisociale que le gouvernement Macron.

Là où nous sommes, il faut discuter interdiction des licenciements, parce que
perdre son emploi, d'autant plus en période de crise, c'est la garantie de
s'appauvrir, de galérer à payer ses factures et à se nourrir. Il faut aussi
discuter embauches définitives des précaires (intérimaires, CDD, prestataires,
etc.). En imposant des avancées concrètes pour une partie des travailleurs, en
mettant en avant l'auto-organisation sans attendre une réponse d'en haut, ce
sont tous les travailleurs qui en sortiront gagnant.



## Sources

- https://www.convergencesrevolutionnaires.org/Contribution-de-la-Fraction-l-Etincelle-a-la-discussion-sur-la-situation?navthem=1
- https://www.convergencesrevolutionnaires.org/Zero-euro-pour-les-heros
- https://www.convergencesrevolutionnaires.org/Qu-est-ce-que-les-revolutionnaires-pourraient-tenter-de-mettre-en-avant
